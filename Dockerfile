FROM node:lts-alpine

RUN yarn global add serve

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install -P

COPY . .

RUN yarn run build

EXPOSE 3000
CMD serve -s dist
