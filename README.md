# tp1

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Build and run container
```
docker build -t vue-devops . && docker run -p 3000:3000 vue-devops
```

Automatic tests, build & deployment

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
